package org.nabsha.camel.giraphe;

import org.junit.Test;
import org.nabsha.camel.giraphe.jolokia.JolokiaClient;

/**
 * Created by nabeel on 21/08/16.
 */
public class GiraphGeneratorTest {

    @Test
    public void testActivityGeneration() throws Exception {
        JolokiaClient client = new JolokiaClient("http://localhost:8181/hawtio/jolokia", null, null);
        GiraphGenerator giraphGenerator = new GiraphGenerator();

        giraphGenerator.generatePumlFromRoutes(client);

    }

}