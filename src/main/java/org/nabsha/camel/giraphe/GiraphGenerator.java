package org.nabsha.camel.giraphe;

import net.sourceforge.plantuml.FileFormat;
import net.sourceforge.plantuml.FileFormatOption;
import net.sourceforge.plantuml.SourceStringReader;
import org.apache.camel.model.RouteDefinition;
import org.apache.commons.io.FileUtils;
import org.nabsha.camel.giraphe.jolokia.JolokiaClient;
import org.nabsha.camel.giraphe.puml.RouteDefinitionPumlActivityImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

/**
 * Created by nabeel on 20/08/16.
 */
public class GiraphGenerator {
    Logger logger = LoggerFactory.getLogger(GiraphGenerator.class);

    public void generatePumlFromRoutes(JolokiaClient client) throws Exception {

        logger.info("Requesting routes from server");
        Map<String, Map<String, RouteDefinition>> allRemoteRoutes = client.getAllRemoteRoutes(true);

        for (String contextName : allRemoteRoutes.keySet()) {
            for (String key: allRemoteRoutes.get(contextName).keySet()) {
                String answer = generateActivityFromRoute(allRemoteRoutes.get(contextName).get(key));

                String filename = getPumlFilePath(contextName, key);

                logger.info("Writing response to file " + filename);
                FileUtils.writeStringToFile(new File(filename), answer, "UTF-8");

                generateSvgFromPuml(filename, getSvgFilePath(contextName, key));
            }
        }
    }

    public boolean generateSvgFromPuml(String pumlFilePath, String svgFilePath) throws IOException {
        String answer = FileUtils.readFileToString(new File(pumlFilePath), "UTF-8");

        File svgFile = new File(svgFilePath);
        svgFile.getParentFile().mkdirs();

        logger.info("Generating svg from " + pumlFilePath);
        SourceStringReader reader = new SourceStringReader(answer);

        String result = reader.generateImage(new FileOutputStream(svgFilePath), new FileFormatOption(FileFormat.SVG));
        if (result == null) {
            logger.error("failed to generated SVG for " + pumlFilePath + ", check the file syntax");
            return false;
        }
        return true;
    }

    private String getPumlFilePath(String contextName, String key) {
        return "puml/" + contextName + "/" + key + ".puml";
    }

    private String getSvgFilePath(String contextName, String key) {
        return "svg/" + contextName + "/" + key + ".svg";
    }

    public String generateActivityFromRoute(RouteDefinition route) {
        Visitor visitor = new RouteDefinitionPumlActivityImpl();

        String puml = visitor.visit(route);
        puml = Utilities.wrapPumlTags(puml);
        return puml;
    }
}
