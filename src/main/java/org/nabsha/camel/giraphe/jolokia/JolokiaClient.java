package org.nabsha.camel.giraphe.jolokia;

import org.apache.camel.model.ModelHelper;
import org.apache.camel.model.RouteDefinition;
import org.apache.commons.io.FileUtils;
import org.jolokia.client.BasicAuthenticator;
import org.jolokia.client.J4pClient;
import org.jolokia.client.exception.J4pException;
import org.jolokia.client.request.J4pExecRequest;
import org.jolokia.client.request.J4pResponse;
import org.jolokia.client.request.J4pSearchRequest;
import org.jolokia.client.request.J4pSearchResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by nabeel on 20/08/16.
 */
public class JolokiaClient {

    public static final String M_BEAN_PATTERN = "org.apache.camel:context=*,type=routes,*";

    private static Logger logger = LoggerFactory.getLogger(JolokiaClient.class);

    private final J4pClient j4pClient;

    public JolokiaClient(String url, String username, String password) {
        if (url == null)
            url = "http://localhost:8181/hawtio/jolokia";

        if (username == null)
            username = "admin";

        if (password == null)
            password = "admin";

        this.j4pClient = J4pClient.url(url)
                .user(username)
                .password(password)
                .authenticator(new BasicAuthenticator().preemptive())
                .connectionTimeout(3000)
                .build();
    }

    public Map<String, Map<String, RouteDefinition>> getAllRemoteRoutes(boolean usePersistence) throws Exception {

        J4pSearchRequest searchRequest = new J4pSearchRequest(M_BEAN_PATTERN);
        J4pSearchResponse searchResponse = j4pClient.execute(searchRequest);

        Map<String, Map<String, RouteDefinition>> contextMap = new LinkedHashMap<>();

        for (ObjectName objectName : searchResponse.getObjectNames()) {
            Map<String, String> names = getNames(objectName.getCanonicalName());
            String contextName = names.get("context");
            String routeName = names.get("name");


            RouteDefinition modelFromXml = null;

            if (usePersistence)
                modelFromXml = find(objectName.getCanonicalName());

            if (modelFromXml == null) {
                modelFromXml = getRemoteRoute(objectName.getCanonicalName(), usePersistence);
            }

            if (contextMap.get(contextName) == null) {
                contextMap.put(contextName, new LinkedHashMap<String, RouteDefinition>());
            } else {
                contextMap.get(contextName).put(routeName, modelFromXml);
            }
        }

        return contextMap;
    }

    private Map<String, String> getNames(String canonicalName) {
        Map<String, String> namesMap = new LinkedHashMap<>();
        String[] toks = canonicalName.replace("org.apache.camel:", "").split(",");
        for (String tok : toks) {
            if (tok.contains("=")) {
                String[] subToks = tok.split("=");
                namesMap.put(subToks[0].replace("\"", ""), subToks[1].replace("\"", ""));
            }
        }
        return namesMap;
    }

    public RouteDefinition getRemoteRoute(String objectName, boolean doPersist) throws JAXBException, MalformedObjectNameException, J4pException, IOException {

        logger.info("Requesting route details for " + objectName + " from remote" + j4pClient);

        J4pExecRequest req = new J4pExecRequest(objectName, "dumpRouteAsXml");
        J4pResponse resp = this.j4pClient.execute(req);
        String xml = resp.getValue().toString();

        if (doPersist) {
            store(objectName, xml);
        }
        return ModelHelper.createModelFromXml(null, xml, RouteDefinition.class);
    }

    private RouteDefinition find(String objectName) {
        Map<String, String> names = getNames(objectName);
        String filename = "cache/" + names.get("context") + "/" + names.get("name");
        try {
            String routeXml = FileUtils.readFileToString(new File(filename), "UTF-8");
            if (routeXml != null) {
                return ModelHelper.createModelFromXml(null, routeXml, RouteDefinition.class);
            }
        } catch (IOException e) {
            logger.warn(filename + " not found");
        } catch (JAXBException e) {
            logger.error("failed to process message" + e);
        }

        return null;
    }

    private void store(String objectName, String xml) throws IOException {
        Map<String, String> names = getNames(objectName);
        String filename = "cache/" + names.get("context") + "/" + names.get("name");
        FileUtils.writeStringToFile(new File(filename), xml, "UTF-8");
    }

}
